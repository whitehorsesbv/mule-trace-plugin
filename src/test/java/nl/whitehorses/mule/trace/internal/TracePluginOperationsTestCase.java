package nl.whitehorses.mule.trace.internal;

import nl.whitehorses.mule.trace.LogAppenderResource;
import nl.whitehorses.mule.trace.TraceLogger;
import org.apache.logging.log4j.LogManager;
import org.junit.Rule;
import org.junit.Test;
import org.mule.functional.junit4.MuleArtifactFunctionalTestCase;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.fail;

/**
 * JUnit test for the Trace Plugin operations.
 */
public class TracePluginOperationsTestCase extends MuleArtifactFunctionalTestCase {

    @Rule
    public LogAppenderResource logAppenderResource = new LogAppenderResource(LogManager.getLogger(TraceLogger.class));

    @Test
    public void executeLogOperation() throws Exception {
        // Trigger logFlow and store payload
        final String payloadValue = ((String) flowRunner("logFlow").run()
                .getMessage()
                .getPayload()
                .getValue());
        // Assert payload
        assertThat(payloadValue, is("Success"));
        // Assert logging
        assertLogging("TracePluginOperationsTestCase_executeLogOperation_expected.log");
    }

    @Test
    public void serializationAndDeserialization() throws Exception {
        // Trigger logFlowWithSerialization and store payload
        final String serializationPayloadValue = ((String) flowRunner("logFlowWithSerialization")
                .run()
                .getMessage()
                .getPayload()
                .getValue());
        // Trigger logFlowWithDeserialization and store payload
        final String deserializationPayloadValue = ((String) flowRunner("logFlowWithDeserialization")
                .withPayload(serializationPayloadValue)
                .run()
                .getMessage()
                .getPayload()
                .getValue());
        // Assert payload
        assertThat(deserializationPayloadValue, is("Success"));
        // Assert logging
        assertLogging("TracePluginOperationsTestCase_serializationAndDeserialization_expected.log");
    }

    @Test
    public void deserializationFailure() {
        try {
            // Trigger logFlowWithDeserialization and store exception
            flowRunner("logFlowWithDeserialization")
                    .withPayload(Base64.getEncoder().encodeToString("INCORRECT PAYLOAD".getBytes()))
                    .run();
            // Assert exception
            fail("Exception expected");
        } catch (final Exception ex) {
            // Assert exception
            assertThat(ex.getMessage(), containsString("Caused by: nl.whitehorses.mule.trace.exception.TracePluginException: Unable to deserialize properties"));
        }
    }

    @Override
    protected String getConfigFile() {
        return "test-mule-config.xml";
    }

    private void assertLogging(final String expectedLogFile) throws URISyntaxException, IOException {
        // Get expected logging
        final Path path = Paths.get(requireNonNull(getClass().getClassLoader().getResource(expectedLogFile)).toURI());
        final Stream<String> lines = Files.lines(path);
        final String expectedLog = lines.collect(Collectors.joining("\n"));
        lines.close();
        // Replace GUID properties with placeholders
        final String logOutput = logAppenderResource.getLogOutput()
                .replaceAll("(\"traceId\": \")(.*)(\")", "$1TRACE-ID-PLACEHOLDER$3")
                .replaceAll("(\"messageId\": \")(.*)(\")", "$1MESSAGE-ID-PLACEHOLDER$3");
        // Assert logging
        assertThat(logOutput, is(expectedLog));
    }

}
