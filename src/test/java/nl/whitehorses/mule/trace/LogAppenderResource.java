package nl.whitehorses.mule.trace;

import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.appender.WriterAppender;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.junit.rules.ExternalResource;

import java.io.CharArrayWriter;

/**
 * Appender resource for testing log output in JUnit tests.
 */
public class LogAppenderResource extends ExternalResource {

    private static final String PATTERN = "%level %msg";

    private final Logger logger;
    private final CharArrayWriter logWriter = new CharArrayWriter();

    private Appender appender;

    /**
     * Constructor.
     *
     * @param logger The logger that needs to be tested.
     */
    public LogAppenderResource(org.apache.logging.log4j.Logger logger) {
        this.logger = (Logger) logger;
    }

    /**
     * Get output that was logged.
     *
     * @return Output that was logged.
     */
    public String getLogOutput() {
        return logWriter.toString();
    }

    @Override
    protected void before() {
        final PatternLayout layout = PatternLayout.newBuilder().withPattern(PATTERN).build();
        appender = WriterAppender.createAppender(
                layout,
                null,
                logWriter,
                getClass().getName(),
                false,
                false
        );
        appender.start();
        logger.addAppender(appender);
    }

    @Override
    protected void after() {
        logger.removeAppender(appender);
    }

}
