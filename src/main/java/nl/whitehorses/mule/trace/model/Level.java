package nl.whitehorses.mule.trace.model;

/**
 * Enumeration containing available log levels.
 */
@SuppressWarnings("unused")
public enum Level {

    /**
     * Log level for trace data.
     */
    TRACE(org.apache.logging.log4j.Level.TRACE),

    /**
     * Log level for debug data.
     */
    DEBUG(org.apache.logging.log4j.Level.DEBUG),

    /**
     * Log level for info data, this is the default {@code MuleSoft} log level.
     */
    INFO(org.apache.logging.log4j.Level.INFO),

    /**
     * Log level for warnings.
     */
    WARNING(org.apache.logging.log4j.Level.WARN),

    /**
     * Log level for errors.
     */
    ERROR(org.apache.logging.log4j.Level.ERROR);

    private final org.apache.logging.log4j.Level log4jLevel;

    /**
     * Default constructor.
     *
     * @param log4jLevel Log4j {@link org.apache.logging.log4j.Level}.
     */
    Level(final org.apache.logging.log4j.Level log4jLevel) {
        this.log4jLevel = log4jLevel;
    }

    /**
     * Get log4j {@link Level}.
     *
     * @return Log4j {@link Level}.
     */
    public org.apache.logging.log4j.Level getLog4jLevel() {
        return this.log4jLevel;
    }

}