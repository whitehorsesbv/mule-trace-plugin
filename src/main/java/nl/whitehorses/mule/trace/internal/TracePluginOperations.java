package nl.whitehorses.mule.trace.internal;

import nl.whitehorses.mule.trace.internal.parameter.LogParameters;
import nl.whitehorses.mule.trace.TraceProperties;

import nl.whitehorses.mule.trace.TraceLogger;
import org.json.JSONObject;
import org.mule.runtime.api.component.location.ComponentLocation;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.runtime.parameter.CorrelationInfo;

import java.io.Serializable;
import java.util.Map;

/**
 * This class defines all operations of the Trace Plugin.
 */
public class TracePluginOperations {

    /**
     * Method to trace log a message.
     *
     * @param properties      Operation parameters.
     * @param location        Instance of {@link LogParameters} that will be injected as parameter automatically.
     * @param correlationInfo Instance of {@link CorrelationInfo} that will be injected as parameter automatically.
     */
    @SuppressWarnings("unused")
    public void log(@ParameterGroup(name = "Log properties") final LogParameters properties, final ComponentLocation location, final CorrelationInfo correlationInfo) {
        final TraceProperties traceProperties = properties.getTraceProperties();
        final JSONObject json = new JSONObject();
        json.put("messageId", correlationInfo.getEventId());
        json.put("traceId", correlationInfo.getCorrelationId());
        json.put("source", traceProperties == null ? null : traceProperties.getSource());
        json.put("destination", traceProperties == null ? null : traceProperties.getDestination());
        json.put("flow", location.getRootContainerName());
        json.put("phase", properties.getPhase());
        final Map<String, Serializable> globalMetaData = traceProperties == null ? null : traceProperties.getMetaData();
        final Map<String, Object> metaData = properties.getMetaData();
        if (globalMetaData != null || metaData != null) {
            final JSONObject jsonMetaData = new JSONObject();
            if (globalMetaData != null) {
                globalMetaData.forEach((key, value) -> jsonMetaData.put(key, value.toString()));
            }
            if (metaData != null) {
                metaData.forEach((key, value) -> jsonMetaData.put(key, value.toString()));
            }
            json.put("metaData", jsonMetaData);
        }
        json.put("message", properties.getMessage());
        TraceLogger.log(properties.getLevel(), json.toString(2));
    }

}
