package nl.whitehorses.mule.trace.model;

/**
 * Enumeration containing all log phases.
 */
@SuppressWarnings("unused")
public enum Phase {

    /**
     * Start of the Mule flow.
     */
    START,

    /**
     * End of the Mule flow.
     */
    END,

    /**
     * Before (outbound) Transport Barrier.
     */
    BEFORE_TRANSPORT,

    /**
     * After (outbound) Transport Barrier.
     */
    AFTER_TRANSPORT,

    /**
     * Exception handler of a flow.
     */
    EXCEPTION,

    /**
     * Other phase in the flow.
     */
    OTHER
    
}
