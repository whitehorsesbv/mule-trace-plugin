package nl.whitehorses.mule.trace;

import nl.whitehorses.mule.trace.model.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Helper class to write log entries.
 */
public class TraceLogger {

    private static final Logger LOG = LogManager.getLogger(TraceLogger.class);

    /**
     * Private constructor. All (public) methods are static.
     */
    private TraceLogger() {
        super();
    }

    /**
     * Write message to log.
     *
     * @param level   Log level.
     * @param message Log message.
     */
    public static void log(final Level level, final String message) {
        LOG.log(level.getLog4jLevel(), message);
    }

}
