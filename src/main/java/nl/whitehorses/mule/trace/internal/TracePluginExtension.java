package nl.whitehorses.mule.trace.internal;

import org.mule.runtime.extension.api.annotation.Extension;
import org.mule.runtime.extension.api.annotation.Operations;
import org.mule.runtime.extension.api.annotation.dsl.xml.Xml;

/**
 * Main class of the Trace Plugin. This is the entry point from which operations etc. are going to be declared.
 */
@Xml(prefix = "trace")
@Extension(name = "Trace Plugin")
@Operations(TracePluginOperations.class)
@SuppressWarnings("unused")
public class TracePluginExtension {
}
