package nl.whitehorses.mule.trace.exception;

/**
 * Implementation of {@link Exception}s that are raised from the Trace Plugin.
 */
public class TracePluginException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Constructor.
     *
     * @param message Error message.
     * @param cause   Cause of the error.
     */
    public TracePluginException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
