package nl.whitehorses.mule.trace.internal.parameter;

import nl.whitehorses.mule.trace.TraceProperties;
import nl.whitehorses.mule.trace.model.Level;
import nl.whitehorses.mule.trace.model.Phase;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;

import java.util.Map;

/**
 * Input properties for {@code log} method of the Trace Plugin.
 */
public class LogParameters {

    @Parameter
    @Expression(value = ExpressionSupport.REQUIRED)
    @Optional(defaultValue = "#[vars.traceProperties]")
    private TraceProperties traceProperties;

    @Parameter
    @Optional(defaultValue = "INFO")
    private Level level;

    @Parameter
    private Phase phase;

    @Parameter
    @Expression(ExpressionSupport.SUPPORTED)
    private String message;

    @Parameter
    @Optional
    @Expression(ExpressionSupport.SUPPORTED)
    @Alias("meta-data-value")
    private Map<String, Object> metaData;

    /**
     * Get trace properties.
     *
     * @return Trace properties.
     */
    @SuppressWarnings("unused")
    public TraceProperties getTraceProperties() {
        return traceProperties;
    }

    /**
     * Set trace properties.
     *
     * @param traceProperties Trace properties.
     */
    @SuppressWarnings("unused")
    public void setTraceProperties(final TraceProperties traceProperties) {
        this.traceProperties = traceProperties;
    }

    /**
     * Get log level
     *
     * @return Log level.
     */
    public Level getLevel() {
        return level;
    }

    /**
     * Set log level.
     *
     * @param level Log level.
     */
    @SuppressWarnings("unused")
    public void setLevel(final Level level) {
        this.level = level;
    }

    /**
     * Get log phase.
     *
     * @return Log phase.
     */
    public Phase getPhase() {
        return phase;
    }

    /**
     * Set log phase.
     *
     * @param phase Log phase.
     */
    @SuppressWarnings("unused")
    public void setPhase(final Phase phase) {
        this.phase = phase;
    }

    /**
     * Get message that needs to be logged.
     *
     * @return Message that needs to be logged.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set message that needs to be logged.
     *
     * @param message Message that needs to be logged.
     */
    @SuppressWarnings("unused")
    public void setMessage(final String message) {
        this.message = message;
    }

    /**
     * Get meta data.
     *
     * @return Meta data.
     */
    public Map<String, Object> getMetaData() {
        return metaData;
    }

    /**
     * Set meta data.
     *
     * @param metaData Meta data.
     */
    @SuppressWarnings("unused")
    public void setMetaData(final Map<String, Object> metaData) {
        this.metaData = metaData;
    }

}
