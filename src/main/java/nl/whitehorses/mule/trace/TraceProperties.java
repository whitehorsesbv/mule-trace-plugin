package nl.whitehorses.mule.trace;

import nl.whitehorses.mule.trace.exception.TracePluginException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Base64;
import java.util.Map;

/**
 * Class that contains the trace data that should be preserved during the (trace) flow.
 */
public class TraceProperties implements Serializable {

    private static final long serialVersionUID = 1L;

    private String source;
    private String destination;
    private Map<String, Serializable> metaData;

    /**
     * Constructor.
     *
     * @param source      Message source.
     * @param destination Message destination.
     * @param metaData    Meta data.
     */
    public TraceProperties(final String source, final String destination, final Map<String, Serializable> metaData) {
        this.source = source;
        this.destination = destination;
        this.metaData = metaData;
    }

    /**
     * Get message source.
     *
     * @return Message source.
     */
    public String getSource() {
        return source;
    }

    /**
     * Set message source.
     *
     * @param source Message source.
     */
    @SuppressWarnings("unused")
    public void setSource(final String source) {
        this.source = source;
    }

    /**
     * Get message destination.
     *
     * @return Message destination.
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Set message destination.
     *
     * @param destination Message destination.
     */
    @SuppressWarnings("unused")
    public void setDestination(final String destination) {
        this.destination = destination;
    }

    /**
     * Get meta data.
     *
     * @return Meta data.
     */
    public Map<String, Serializable> getMetaData() {
        return metaData;
    }

    /**
     * Set meta data.
     *
     * @param metaData Meta data.
     */
    @SuppressWarnings("unused")
    public void setMetaData(final Map<String, Serializable> metaData) {
        this.metaData = metaData;
    }

    /**
     * Get the serialized value of a {@link TraceProperties} instance. This can be used to pass the properties to
     * other flows, for example via an HTTP header. In the other flow, the properties can be read again using the
     * {@link #deserializeProperties(String)} method.
     *
     * @param traceProperties The {@link TraceProperties} object that needs to be serialized.
     * @return Base64 encoded {@link String} that represents the {@link TraceProperties} object.
     * @throws TracePluginException Exception thrown when serialization failed.
     */
    @SuppressWarnings({"unused", "WeakerAccess"})
    public static String serializeProperties(final TraceProperties traceProperties) throws TracePluginException {
        try (final ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(traceProperties);
            return Base64.getEncoder().encodeToString(baos.toByteArray());
        } catch (final IOException ex) {
            throw new TracePluginException("Unable to serialize properties", ex);
        }
    }

    /**
     * Factory method to deserialize existing {@link TraceProperties} instance, that was encoded earlier using the
     * {@link #serializeProperties(TraceProperties)} method.
     *
     * @param traceProperties Serialized instance of the {@link TraceProperties}.
     * @return {@link TraceProperties} instance.
     * @throws TracePluginException Exception thrown when deserialization failed.
     */
    @SuppressWarnings({"unused", "WeakerAccess"})
    public static TraceProperties deserializeProperties(final String traceProperties) throws TracePluginException {
        final byte[] decodedValue = Base64.getDecoder().decode(traceProperties);
        try (final ByteArrayInputStream bais = new ByteArrayInputStream(decodedValue);
             final ObjectInputStream ois = new ObjectInputStream(bais)) {
            return (TraceProperties) ois.readObject();
        } catch (final IOException | ClassNotFoundException ex) {
            throw new TracePluginException("Unable to deserialize properties", ex);
        }
    }
}
